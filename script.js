
// 1. შეადგინეთ მოცემული ინფორმაციისთვის შესაბამისი დატის სტრუქტურა. (შეეცადეთ მონაცემები მაქსიმალურად ოპტიმალურად გადაანაწილოთ და შეინახოთ)
const tourists = [];
tourists[0] = {
    firstName: 'Mark',
    age: '19',
    cities: ['თბილისი', 'ლონდონი', 'რომი', 'ბერლინი'],
    expenses: [120, 200, 150, 140]

}
tourists[1] = {
    firstName: 'Bob',
    age: '21',
    cities: ['მაიამი', 'მოსკოვი', 'ვენა', 'რიგა', 'კიევი'],
    expenses: [90, 240, 100, 76, 123]
}
tourists[2] = {
    firstName: 'Sam',
    age: '22',
    cities: ['თბილისი', ' ბუდაპეშტი', 'ვარშავა', 'ვილნიუსი'],
    expenses: [118, 95, 210, 236]
}
tourists[3] = {
    firstName: 'Anna',
    age: '20',
    cities: ['ნიუ იორკი', 'ათენი', 'სიდნეი', 'ტოკიო'],
    expenses: [100, 240, 50, 190]
}
tourists[4] = {
    firstName: 'Alex',
    age: '23',
    cities: ['პარიზი', 'თბილისი', 'მადრიდი', 'მარსელი', 'მინსკი'],
    expenses: [96, 134, 76, 210, 158]
}

// 2. გაარკვიეთ თითოეული სტუდენტისთვის არის თუ არა ის ზრდასრული (21+, ზრდასრულში 21 წელიც იგუისხმება), პასუხის მიხედვით შექმენით შესაბამისი key და შეინახეთ შესაბამისი სტუდენტის ინფოში როგორც boolean ტიპის ცვლადი.

for(let i = 0; i < tourists.length; i++){
    tourists[i]['adult'] = false;
    if(tourists[i].age >= 21){
        tourists[i]['adult']= true;
    }
}
// 3. გაარკვიეთ თითოეული სტუდენტისთვის არის თუ არა ის ნამყოფი საქართველოში და პასუხის მიხედვით წინა დავალების ანალოგიურად შექმენით შესაბამისი key და შეინახეთ შესაბამისი სტუდენტის ინფოში ამჯერად რაიმე სტინგის სახით (ნამყოფია, არაა ნამყოფი და ა.შ.).

for(let i = 0; i < tourists.length; i++){
    tourists[i]['visitedGeorgia'] = 'არ არის ნამყოფი';
    for(let j = 0; j < tourists[i].cities.length; j++){
        if(tourists[i].cities[j] === 'თბილისი'){
            tourists[i]['visitedGeorgia'] = 'ნამყოფია';
        }
    }
}
// 4. დაითვალეთ თითოეული ტურისტისთვის ჯამში რა თანხა დახარჯა მოგზაურობისას.

for(let i = 0; i < tourists.length; i++){
    tourists[i]['sumExpenses'] = 0;
    for(let j = 0; j < tourists[i].expenses.length; j++){
        tourists[i]['sumExpenses'] += tourists[i].expenses[j]
    }
}
// 5. დაითვალეთ თითოეული ტურისტისთვის საშუალოდ რა თანხა დახარჯა მოგზაურობისას.

for(let i = 0; i < tourists.length; i++){
    tourists[i]['avarageExpenses'] = 0;
    for(let j = 0; j < tourists[i].expenses.length; j++){
        tourists[i]['avarageExpenses'] += tourists[i].expenses[j] / tourists[i].expenses.length
    }
}

// 6. გამოავლინეთ ყველაზე "მხარჯველი" ტურისტი, დალოგეთ ვინ დახარჯა ყველაზე მეტი და რამდენი.

let maxExpenses = tourists[0].sumExpenses
let maxspender = tourists[0].firstName
for(let i = 0; i < tourists.length; i++) {
    if(tourists[i].sumExpenses > maxExpenses) {
        maxExpenses = tourists[i].sumExpenses
        maxspender = tourists[i].firstName
    }
}

console.log(`მეტი დახარჯა ${maxspender}-მა ${maxExpenses}`)

console.log(tourists)
